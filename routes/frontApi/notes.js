const Router = require("koa-router");
const router = new Router();
const NotesNavsModel = require("../../model/notesNavsModel");
const NotesModel = require("../../model/notesModel");
const fs = require("fs").promises;
const path = require("path");
const redisClient = require("../../redis");
const mark = 'a'
//请求notes列表
router.get("/get-nav", async (ctx, next) => {
  let navList = [];
  navList = await NotesNavsModel.find({}).then((data) => data);
  ctx.body = {
    code: "0000",
    msg: "栏目获取成功",
    data: navList.map((item) => {
      return {
        nav_id: item.nav_id,
        title: item.title,
      };
    }),
  };
});

//返回文章列表
router.post("/list-article", async (ctx, next) => {
  const { nav_id = '', page = 1, pageSize = 5,sort = ''} = ctx.request.body;
  if (isNaN(Number(page)) || isNaN(Number(pageSize))) {
    ctx.body = {
      code: "1003",
      msg: "参数错误",
      data: null,
    };
  }
  const reqPage = Number(page);
  const reqPageSize = Number(pageSize);
  if (nav_id === "" && sort === "") {
    ctx.body = {
      code: "1002",
      msg: "id和sort不能同时为空",
      data: null,
    };
  }
  let sortObj = {}
  let findObj = {}
  let maxCount = 0
  if (nav_id !== "") {
    findObj.nav_id = nav_id

    maxCount = await redisClient.get("NotesCount").then((data) => {
      if (data === null) {
        return -1;
      } else {
        return JSON.parse(data).filter((e) => e.nav_id === nav_id)[0].maxCount;
      }
    });
    if (maxCount === -1) {
      maxCount = (await NotesModel.find({ nav_id: nav_id })
        .then((data) => data))
        .length;
    }
  }

  if (sort === "proportion") {
    sortObj.proportion = 1;
    maxCount = pageSize
  }

  const result = await NotesModel.find(findObj)
    .sort(sortObj)
    .skip((reqPage - 1) * reqPageSize)
    .limit(reqPageSize)
    .then((data) => data);
  ctx.body = {
    code: "0000",
    msg: "文章列表获取成功",
    maxCount: maxCount,
    data: result.map((item) => {
      return {
        nav_id: item.nav_id,
        title: item.title,
        time: item.time,
        article_id: item.article_id,
        watchNum: item.watchNum,
      };
    }),
  };
});
//返回文章详情
router.post("/detail-article", async (ctx, next) => {
  const { article_id } = ctx.request.body;
  // Model.find({})
  if (article_id === "") {
    ctx.body = {
      code: "1002",
      msg: "article_id不能为空",
      data: null,
    };
  }
  const result = await NotesModel.find({ article_id: article_id }).then(
    (data) => data
  );

  if (result.length > 0) {
    const current = result[0];

    NotesModel.findOneAndUpdate(
      { article_id: article_id },
      {
        $set: {
          watchNum: String(Number(current.watchNum) + 1),
        },
      }
    ).then((data) => data);
    try {
      const fsFile = await fs.readFile(current.filePath, "utf-8");
      ctx.body = {
        code: "0000",
        msg: "",
        data: {
          nav_id: current.nav_id,
          title: current.title,
          time: current.time,
          article_id: current.article_id,
          watchNum: current.watchNum,
          file: fsFile,
        },
      };
    } catch (error) {
      ctx.body = {
        code: "1003",
        msg: "文件读取失败",
        data: {
          nav_id: current.nav_id,
          title: current.title,
          time: current.time,
          article_id: current.article_id,
          watchNum: current.watchNum,
          file: '',
        },
      };
    }
  }
});

//上传评论
router.post("/notes-comment-upload", async (ctx, next) => {
  const { id, cid, commentBody, commentName } = ctx.request.body;
  const aid = mark + id
  // let isReply = false;
  // await Comments.setComments(id, cid, commentBody, commentName,(res)=>{

  //   ctx.body = res
  // })
  ctx.body = await Comments.setComments(aid, cid, commentBody, commentName);
});

//获取评论
router.post("/notes-comment", async (ctx, next) => {
  const { id } = ctx.request.body;
  if (id === "") {
    ctx.body = {
      code: "1001",
      msg: "参数错误",
      data: null,
    };
  }
  const aid = mark + id

  await addWatch(aid);
  ctx.body = await Comments.getComments(aid);

});

module.exports = router;
