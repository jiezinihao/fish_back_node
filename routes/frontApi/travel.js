const Router = require("koa-router");
const router = new Router();
const TravelListModel = require("../../model/travelListModel");
const redisClient = require("../../redis");

async function addWatch(id, watchNum) {
  let travel = await TravelListModel.find({ travel_id: id }).then((data) => data);
  if (travel.length === 0) {
    return;
  }
  await TravelListModel.findOneAndUpdate(
    { travel_id: id },
    {
      $set: {
        watchNum: String(Number(travel[0].watchNum) + 1),
      },
    }
  )
}
//请求travel列表
router.post("/travel-list", async (ctx, next) => {
  let travelList = [];
  const { page = 1, pageSize = 5 ,sort} = ctx.request.body;
  if (isNaN(Number(page)) || isNaN(Number(pageSize))) {
    ctx.body = {
      code: "1003",
      msg: "参数错误",
      data: null,
    };
  }
  const reqPage = Number(page);
  const reqPageSize = Number(pageSize);

  let maxCount = await redisClient.get("TravelCount").then((data) => {
    if (data === null) {
      return -1;
    } else {
      return Number(data);
    }

  });
//如果没设置maxCount，则获取并且存储到redis中
  if (maxCount === -1) {
    maxCount = await TravelListModel.find({}).then((data) => data.length);
    await redisClient.set("TravelCount", maxCount);
  }
  let sortObj = {}
  if(sort === "proportion") {
    sortObj.proportion = -1
  }
  
  travelList = await TravelListModel.find({})
  .sort(sortObj)
  .skip((reqPage - 1) * reqPageSize)
  .limit(reqPageSize)
  .then((data) => data);
  
  ctx.body = {
    code: "0000",
    msg: "栏目获取成功",
    maxCount: maxCount,
    data: travelList.map((item) => {
      return {
        title: item.title,
        body: item.body,
        title: item.title,
        time: item.time,
        travel_id: item.travel_id,
        thumb: item.thumb,
        watchNum: item.watchNum,
        imgList: item.imgList.map((itemImg) => {
          return {
            url: itemImg.url,
            name: itemImg.name,
            uid: itemImg.uid,
          };
        }),
      };
    }),
  };
});
//获得详情
router.post("/travel-detail", async (ctx, next) => {
  const { id } = ctx.request.body;
  let travel = await TravelListModel.find({ travel_id: id }).then((data) => data)

  let res = {}
  if (travel.length > 0) {
    travel = travel[0]
    res = {
      code: "0000",
      msg: "栏目获取成功",
      data: {
        title: travel.title,
        body: travel.body,
        title: travel.title,
        time: travel.time,
        travel_id: travel.travel_id,
        thumb: travel.thumb,
        watchNum: travel.watchNum,
        imgList: travel.imgList.map((itemImg) => {
          return {
            url: itemImg.url,
            name: itemImg.name,
            uid: itemImg.uid,
          };
        })
      }
    };
    addWatch(id, travel.watchNum)
  } else {
    res = {
      code: "0001",
      msg: "栏目获取失败",
      data: null
    }
  }
  ctx.body = res;
})


module.exports = router;
