const Router = require("koa-router");
const router = new Router();
const TravelListModel = require("../../model/travelListModel");
const getNextSequenceValue = require("../../func/getNextSequenceValue");
const fs = require("fs").promises;
const path = require("path");

//请求travel列表
router.get("/travel-list", async (ctx, next) => {
  let travelList = await TravelListModel.find({}).then((data) => data);

  ctx.body = {
    code: "0000",
    msg: "栏目获取成功",
    data: travelList.map((item) => {
      return {
        travel_id: item.travel_id,
        title: item.title,
        time: item.time,
        thumb: item.thumb,
        body: item.body,
        watchNum:item.watchNum,
        proportion:item.proportion,
        imgList: item.imgList.map((imgItem) => {
          return {
            uid: imgItem.uid,
            url: imgItem.url,
            name: imgItem.name,
          };
        }),
      };
    }),
  };
});
//上传旅行文件
router.post("/upload_travel_file", async (ctx, next) => {
  // const params = ctx.request.body;
  const file = ctx.request.files.file;
  const filePath = file.filepath.replace(/\\/g, "/");
  const host = process.env["IPHOST"]
  const baseURL = host
  const newFilePath = path.resolve(
    __dirname,
    "../../file/travel-img/" + file.newFilename
  );
  await fs.rename(filePath, newFilePath);
  ctx.body = {
    code: "0000",
    msg: "文件上传成功",
    data: { file: baseURL + "/travel-img/" + file.newFilename },
  };
});

//上传栏目
router.post("/travel-upload", async (ctx, next) => {
  const {
    title,
    body,
    thumb,
    time,
    imgList,
    ident,
    travel_id,
    watchNum,
    proportion,
  } = ctx.request.body;
  if (isNaN(Number(proportion)) || isNaN(Number(watchNum))) {
    ctx.body = {
      code: "1004",
      msg: "参数类型错误",
      data: null,
    };
  }
  
  if (!title || !body || !thumb || !time || imgList.length === 0) {
    ctx.body = {
      code: "1001",
      msg: "参数错误",
      data: null,
    };
    return;
  }

  if (ident === "edit" && travel_id) {
    let arument = {
      title,
      body,
      thumb,
      time,
      imgList,
      travel_id,
      watchNum:Number(watchNum),
      proportion:Number(proportion),
    };
    await TravelListModel.findOneAndUpdate({ travel_id }, arument, {
      new: true,
      upsert: true,
    })
      .then((data) => {
        ctx.body = {
          code: "0000",
          msg: "栏目修改成功",
          data: null,
        };
      })
      .catch((err) => {
        ctx.body = {
          code: "1002",
          msg: "数据库错误",
          data: JSON.stringify(err),
        };
      });
  } else {
    let travel = new TravelListModel();
    travel.title = title;
    travel.body = body;
    travel.thumb = thumb;
    travel.time = time;
    travel.imgList = imgList;
    travel.watchNum = watchNum;
    travel.proportion = Number(proportion);
    travel.travel_id = await getNextSequenceValue("travel");
    await travel.save().then((data) => {
      ctx.body = {
        code: "0000",
        msg: "",
        data: data,
      };
    });
  }
});

//删除栏目
router.post("/travel-delete", async (ctx, next) => {
  const { travel_id } = ctx.request.body;

  if (travel_id === null || travel_id === undefined || travel_id === "") {
    ctx.body = {
      code: "1001",
      msg: "ID不能为空",
      data: null,
    };
    return;
  }

  const travelList = await TravelListModel.find({}).then((data) => data);
  if (travelList.length < 2) {
    ctx.body = {
      code: "1003",
      msg: "禁止将栏目全部删除",
      data: null,
    };
    return;
  }

  const result = await TravelListModel.deleteOne({ travel_id }).then(
    (data) => data
  );
  if (result.deletedCount > 0) {
    ctx.body = {
      code: "0000",
      msg: "栏目删除成功",
      data: null,
    };
  } else {
    ctx.body = {
      code: "1004",
      msg: "栏目删除失败，未找到栏目",
      data: null,
    };
  }
});

//获取旅行列表

module.exports = router;
