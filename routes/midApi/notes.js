const Router = require("koa-router");
const router = new Router();
const NotesNavsModel = require("../../model/notesNavsModel");
const getNextSequenceValue = require("../../func/getNextSequenceValue");
const fs = require("fs").promises
const path = require("path");
const NotesModel = require("../../model/notesModel");
const redisClient = require("../../redis");

/* GET home page. */
//请求notes列表
router.get("/get-nav", async (ctx, next) => {
  let navList = [];

  navList = await NotesNavsModel.find({}).then((data) => data);
  ctx.body = {
    code: "0000",
    msg: "栏目获取成功",
    data: navList.map((item) => {
      return {
        nav_id: item.nav_id,
        title: item.title,
      };
    }),
  };
});

const editArticle = async (ctx) => {
  const { title, article_id, time,watchNum = 0 ,proportion} = ctx.request.body;
  const result = await NotesModel.findOne({ article_id: article_id }).then(
    (data) => data
  );
  await NotesModel.findOneAndReplace(
    { article_id: article_id },
    {
      article_id: result.article_id,
      filePath: result.article_id,
      nav_id: result.nav_id,
      title,
      time,
      watchNum,
      proportion:Number(proportion)
    }
  )
    .then((data) => {
      ctx.body = {
        code: "0000",
        msg: "文章修改成功",
        data: null,
      };
    })
    .catch((err) => {
      ctx.body = {
        code: "1001",
        msg: "文章修改失败" + JSON.stringify(err),
        data: null,
      };
    });
};

//增加或修改栏目接口
router.post("/add-nav", async (ctx, next) => {
  const params = ctx.request.body;
  if (!params.id || !params.title) {
    ctx.body = {
      code: "1001",
      msg: "参数不能为空",
      data: null,
    };
  }
  //修改逻辑
  if (params.id === "-1" || params.id === -1) {
    let notesNavsModel = NotesNavsModel();
    notesNavsModel.nav_id = await getNextSequenceValue("notesNav");
    notesNavsModel.title = params.title;
    await notesNavsModel
      .save()
      .then((data) => {
        ctx.body = {
          code: "0000",
          msg: "栏目创建成功",
          data: null,
        };
      })
      .catch((err) => {
        ctx.body = {
          code: "1002",
          msg: "数据库错误",
          data: JSON.stringify(err),
        };
      });
  } else {
    await NotesNavsModel.findOneAndUpdate(
      { nav_id: params.id },
      { title: params.title },
      {
        new: true,
        upsert: true,
      }
    )
      .then((data) => {
        ctx.body = {
          code: "0000",
          msg: "栏目修改成功",
          data: null,
        };
      })
      .catch((err) => {
        ctx.body = {
          code: "1002",
          msg: "数据库错误",
          data: JSON.stringify(err),
        };
      });
  }
});

//删除栏目接口
router.post("/delete-nav", async (ctx, next) => {
  const { id, password } = ctx.request.body;

  if (password !== "1275401327") {
    ctx.body = {
      code: "1004",
      msg: "管理员密码错误",
      data: null,
    };
    return
  }

  if (id === null || id === undefined || id === "") {
    ctx.body = {
      code: "1001",
      msg: "ID不能为空",
      data: null,
    };
  }
  const navList = await NotesNavsModel.find({}).then((data) => data);
  if (navList.length < 2) {
    ctx.body = {
      code: "1003",
      msg: "禁止将栏目全部删除",
      data: null,
    };
    return;
  }
  const result = await NotesNavsModel.deleteOne({ nav_id: id }).then(
    (data) => data
  );
  if (result.deletedCount > 0) {
    ctx.body = {
      code: "0000",
      msg: "栏目删除成功",
      data: null,
    };
  } else {
    ctx.body = {
      code: "1004",
      msg: "栏目删除失败，未找到栏目",
      data: null,
    };
  }
});

//上传文章文件
router.post("/upload_article_file", async (ctx, next) =>{
  // const params = ctx.request.body;

  const file = ctx.request.files.file;
  const filePath = file.filepath.replace(/\\/g, "/");
  const newFilePath = path.resolve(__dirname,'../../file/markdown/' + file.newFilename)
  await fs.rename(filePath,newFilePath)
  ctx.body = {
    code: "0000",
    msg: "文件上传成功",
    data: newFilePath,
  };
});

//文章上传接口
router.post("/upload-article", async (ctx, next) => {
  const { title, time, filePath, nav_id, article_id,watchNum ,proportion} = ctx.request.body;

  if (title === "" || time === "" ||  nav_id === "") {
    ctx.body = {
      code: "1001",
      msg: "文章上传失败,请检查参数",
      data: null,
    };
    return;
  }
  if (isNaN(Number(proportion)) || isNaN(Number(watchNum))) {
    ctx.body = {
      code: "1004",
      msg: "参数类型错误",
      data: null,
    };
  }
  //修改逻辑
  if (article_id !== "") {
    await editArticle(ctx);
    return;
  }

  await fs.access(filePath, fs.F_OK, (err) => {
    if (err) {
      ctx.body = {
        code: "1002",
        msg: "未找到上传文件",
        data: null,
      };
      return;
    }
  });

  let notesModel = NotesModel();
  notesModel.title = title;
  notesModel.time = time;
  notesModel.filePath = filePath;
  notesModel.nav_id = nav_id;
  notesModel.watchNum = watchNum;
  notesModel.proportion = Number(proportion)
  
  notesModel.article_id = await getNextSequenceValue("notes");
  await notesModel
    .save()
    .then((data) => {
      ctx.body = {
        code: "0000",
        msg: "文章创建成功",
        data: null,
      };
    })
    .catch((err) => {
      ctx.body = {
        code: "1002",
        msg: "数据库错误",
        data: JSON.stringify(err),
      };
    });
});

//返回文章列表
router.post("/list-article", async (ctx, next) => {
  const {nav_id} = ctx.request.body;
  if (nav_id === "") {
    ctx.body = {
      code: "1002",
      msg: "id不能为空",
      data: null,
    };
  }
  const result = await NotesModel.find({nav_id:nav_id})
  .then((data) => data);
  ctx.body = {
    code: "0000",
    msg: "文章列表获取成功",
    data: result.map((item) => {
      return {
        nav_id: item.nav_id,
        title: item.title,
        time: item.time,
        article_id: item.article_id,
        watchNum:item.watchNum,
        proportion:item.proportion
      };
    }),
  };
});

//删除文章
router.post("/delete-article", async (ctx, next) => {
  const article_id = ctx.request.body.article_id;
  if (article_id === "") {
    ctx.body = {
      code: "1001",
      msg: "article_id不能为空",
      data: null,
    };
  }
  await NotesModel.deleteOne({ article_id: article_id })
    .then((data) => {
      if (data.deletedCount > 0) {
        ctx.body = {
          code: "0000",
          msg: "文章删除成功",
          data: null,
        };
      } else {
        ctx.body = {
          code: "1003",
          msg: "未找到该文章",
          data: null,
        };
      }
    })
    .catch((err) => {
      ctx.body = {
        code: "1002",
        msg: "文章删除失败" + JSON.stringify(err),
        data: null,
      };
    });
});

module.exports = router;
