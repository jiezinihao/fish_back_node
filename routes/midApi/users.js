const UserModel = require("../../model/userModel");
const jwt = require("jsonwebtoken");
const passwordSwift = "meow";
const Router = require("koa-router");
const router = new Router();
const Captcha = require("../../func/caotcha");
const redisClient = require("../../redis");



router.post("/login", async (ctx, next) => {
  // console.log(ctx.query,ctx.querystring);
  // ctx.body = ['post',ctx.request.body]
  const userMsg = ctx.request.body;
  if (!userMsg.name || !userMsg.password || !userMsg.code) {
    ctx.body = {
      code: "1004",
      msg: "输入参数不能为空",
      data: null,
    };
    return;
  }
  let captchaPass = true;
  await redisClient.get(userMsg.code.toLowerCase()).then((val) => {
    if (val === null) {
      captchaPass = false;
    } else {
      redisClient.del(val);
    }
  });
  if (!captchaPass) {
    ctx.body = {
      code: "1003",
      msg: "验证码错误",
      data: null,
    };
    return;
  }
  await UserModel.findOne({ name: userMsg.name }).then((data) => {
    if (data === null) {
      // ctx.body = "无此用户"
      ctx.body = {
        code: "1001",
        msg: "无此用户",
        data: null,
      };
    } else if (data.password === userMsg.password) {
      let token = jwt.sign(
        {
          username: userMsg.name,
          _id: data._id,
        },
        passwordSwift,
        {
          expiresIn: 60,
        }
      );
      ctx.body = {
        code: "0000",
        msg: "登陆成功",
        data: token,
      };
    }
  });
});

router.post("/register", async (ctx, next) => {
  const userMsg = ctx.request.body;
  //注册暂时关闭
  // return;
  const result = await UserModel.findOne({ name: userMsg.name })
    .then((data) => data)
    .catch((err) => {
      ctx.body = {
        code: "1002",
        msg: "注册失败",
        data: null,
      };
    });
  if (result == null) {
    let userModel = new UserModel();
    userModel.name = userMsg.name;
    userModel.password = userMsg.password;
    await userModel.save().then((data) => {
      ctx.body = {
        code: "0000",
        msg: "注册成功",
        data: null,
      };
    });
  } else {
    ctx.body = {
      code: "1001",
      msg: "用户已被注册",
      data: null,
    };
  }
});
module.exports = router;
