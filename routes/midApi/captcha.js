const Router = require("koa-router");
const router = new Router();
const Captcha = require("../../func/caotcha")
const expire = 600;
const redisClient = require("../../redis");

//发送验证吗
router.get("/captcha", async (ctx, next) => {
    const captch = Captcha();
    captch.text = captch.text.toLowerCase();
    await redisClient
      .set(captch.text, captch.text)
      .then((val) => {
        redisClient.expire(captch.text, expire);
        ctx.body = {
            data:captch.data,
            code:"0000",
            uuid:"",
            msg:""
        };
      })
      .catch((err) => {
        ctx.body = {
            data:null,
            code:"1001",
            uuid:null,
            msg:err
        };
      });
  });
  module.exports = router;
