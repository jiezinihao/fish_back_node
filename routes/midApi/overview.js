const OverviewModel = require("../../model/overviewModel");
const Router = require("koa-router");
const router = new Router();
router.get("/get-overview", async (ctx, next) => {
  let overviewList = [];
  const maxLength = 16;

  overviewList = await OverviewModel.find()
    .sort({ _id: -1 })
    .limit(maxLength)
    .then((data) => data);

  ctx.body = {
    code: "0000",
    msg: "栏目获取成功",
    data: overviewList.map((item) => {
      return {
        notesWatch: item.notesWatch,
        travelWatch: item.travelWatch,
        dayOfNotesWatch:item.dayOfNotesWatch,
        dayOfTravelWatch:item.dayOfTravelWatch,
        date: item.date,
      };
    }),
  };
});
module.exports = router;

