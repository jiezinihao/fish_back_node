const Router = require("koa-router")
const baseUrl = process.env["BASEURL"] || ''
const router = new Router({ prefix: "/node" })
const userRouter = require("./midApi/users")
const captchaRouter = require("./midApi/captcha")
const notesRouter = require("./midApi/notes")
const travelRouter = require("./midApi/travel")
const overViewRouter = require("./midApi/overview")

const notesFrontRouter = require("./frontApi/notes")
const travelFrontRouter = require("./frontApi/travel")

//公用部分
const publicRouter = require("./public/comments")
router.use("/public/comments",publicRouter.routes(),publicRouter.allowedMethods);



router.use("/user",userRouter.routes(),userRouter.allowedMethods)
router.use("/captcha",captchaRouter.routes(),captchaRouter.allowedMethods);


//中台
router.use("/notes-mid",notesRouter.routes(),notesRouter.allowedMethods)
router.use("/travel-mid",travelRouter.routes(),travelRouter.allowedMethods)
router.use("/overview-mid",overViewRouter.routes(),overViewRouter.allowedMethods)


//前台
router.use("/notes",notesFrontRouter.routes(),notesFrontRouter.allowedMethods)
router.use("/travel",travelFrontRouter.routes(),travelFrontRouter.allowedMethods)

module.exports = router