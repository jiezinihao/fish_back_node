const Router = require("koa-router");
const router = new Router();
const CommentsModel = require("../../model/commentsModel");
const HotCommentsModel = require("../../model/hotCommentsModel");
const getNextSequenceValue = require("../../func/getNextSequenceValue");
const dayjs = require("dayjs");
const returnNowDate = () => {
  return dayjs(new Date()).format("YYYY/MM/DD");
};
//评论为单独表存储，使用mark区分类型，a为文章，t为旅行日记，m为网站留言
const markList = ['a', 't', 'm'];
const setComments = async (aid, cid, mark, commentBody, commentName) => {
  let isReply = false;
  let res = ""
  if (aid === "" || commentBody === "" || commentName === "") {
    res = {
      code: "1001",
      msg: "参数错误",
      data: null,
    };
  }

  //如果存在cid，则是回复评论
  if (cid !== "") {
    isReply = true;
  }
  const beforeList = await getCommentsList(aid, '', mark)
  
  //如果存在此关联AID，是增加评论
  if (beforeList !== null) {
    let comments = {
      commentBody,
      commentName,
      createTime: returnNowDate(),
      icon: '',
      isHot: '0',
      cid: await getNextSequenceValue("comment-cid"),
    };
    let argument = {
      id: beforeList.id,
      aid: beforeList.aid,
      mark: beforeList.mark,
    };

    if (isReply) {
      let commentList = [
        ...beforeList.commentList.map((item) => {
          if (item.cid === cid) {
            return {
              commentBody: item.commentBody,
              commentName: item.commentName,
              cid: item.cid,
              createTime: item.cid.createTime,
              icon: item.icon,
              isHot: item.icon,
              commentBack: [...item.commentBack, comments],
            };
          } else {
            return item;
          }
        }),
      ];
      argument.commentList = commentList;

    } else {
      argument.commentList = [...beforeList.commentList, comments]
    }
    await CommentsModel.findOneAndUpdate(
      { aid: aid },
      argument,
      {
        new: true,
        upsert: true,
      }
    ).then((data) => {
      res = {
        code: "0000",
        msg: "评论成功",
        data: null,
      }
    });
  } else {
    if(mark === 'm'){
      aid = '99'
    }
    let commentModel = new CommentsModel();
    let comments = {
      commentBody,
      commentName,
      createTime: returnNowDate(),
      icon: '',
      isHot: '0',//默认0
      cid: await getNextSequenceValue("comment-cid"),
    };
    commentModel.id = await getNextSequenceValue("comment-id");
    commentModel.aid = aid;
    commentModel.mark = mark;
    commentModel.commentList = comments;
    const result = await commentModel.save().then((data) => data);
    if (result.aid === aid) {
      res = {
        code: "0000",
        msg: "评论成功",
        data: null,
      };
    } else {
      res = {
        code: "1003",
        msg: "评论失败",
        data: null,
      };
    }
  }
  return res;
}

const getComments = async (aid, mark) => {
  let res = ''
  const result = await getCommentsList(aid, '', mark)
  if (result !== null && result.commentList.length > 0) {
    res = {
      code: "0000",
      msg: "",
      data: result.commentList.map((item) => {
        return {
          commentBody: item.commentBody,
          commentName: item.commentName,
          cid: item.cid,
          createTime: item.createTime,
          icon: item.icon,
          isHot: item.isHot,
          commentBack: item.commentBack.map((item1) => {
            return {
              commentBody: item1.commentBody,
              commentName: item1.commentName,
              cid: item1.cid,
            };
          }),
        };
      }),
    };
  } else {
    res = {
      code: "0000",
      msg: "",
      data: [],
    };
  }
  return res
}

const getCommentsList = async (aid, cid = '', mark) => {
  let comments = [];
  const list = await CommentsModel.findOne({
    aid: aid,
    mark: mark
  }).then((data) => data);
  //获取单条评论评论
  if (cid !== "") {
    let comment = []
    list.commentList.forEach((item) => {
      if (item.cid === cid) {
        comment.push(item)
      }
    })
    comments = comment
  } else {
    comments = list
  }
  return comments
}

//查询推荐评论列表

const getHotComment = async (cid) => {
  const result = await HotCommentsModel.findOne({
    cid: cid
  }).then((data) => data);

  return result
}
//删除推荐评论
const deleteHotComment = async (cid) => {
  const result = await HotCommentsModel.deleteOne({
    cid: cid
  }).then((data) => data);
  return result
}

//获取热门评论
const getHotComments = async () => {
  const res = await HotCommentsModel.find().then((data) => data);
  return res.map(item=>{
    return {
      commentBody: item.commentBody,
      commentName: item.commentName,
      aid: item.aid,
      cid:item.cid,
      createTime: item.createTime,
      icon: item.icon,
      mark: item.mark,
      title: item.title,
    }
  })  
  
}

//上传评论
router.post("/upload-comments", async (ctx, next) => {
  const { id, cid, mark, commentBody, commentName } = ctx.request.body;
  if (markList.indexOf(mark) < 0) {
    ctx.body = {
      code: "1001",
      msg: "参数错误",
      data: null,
    };
  }
  const aid = id;
  // let isReply = false;
  // await Comments.setComments(id, cid, commentBody, commentName,(res)=>{

  //   ctx.body = res
  // })
  ctx.body = await setComments(aid, cid, mark, commentBody, commentName);
})


//获取评论列表
router.post("/get-comments", async (ctx, next) => {
  const { id, mark } = ctx.request.body;
  if (id === "" || markList.indexOf(mark) < 0) {
    ctx.body = {
      code: "1001",
      msg: "参数错误",
      data: null,
    };
  }
  const aid = id;
  ctx.body = await getComments(aid, mark);
})

//删除评论
router.post("/delete-comments", async (ctx, next) => {
  const { id, cid, mark } = ctx.request.body;
  if (id === "" || markList.indexOf(mark) < 0) {
    ctx.body = {
      code: "1001",
      msg: "参数错误",
      data: null,
    };
  }
  const aid = id;

  await CommentsModel.findOneAndUpdate(
    { aid: aid },
    { $pull: { "commentList": { "cid": cid } } },
  ).then(data => {
    ctx.body = {
      code: "0000",
      msg: "删除成功",
      data: null,
    };
  })

})

//设置或取消评论为热门
router.post("/set-comments-hot", async (ctx, next) => {
  const { cid, aid, mark, title } = ctx.request.body;
  let res = {}
  if (cid === "", aid === "", title === "" || markList.indexOf(mark) < 0) {
    ctx.body = {
      code: "1001",
      msg: "参数错误",
      data: null,
    };

  }

  const hotResult = await getHotComment(cid).then((data) => data);
  if (hotResult !== null) {
    const isHot = hotResult.isHot
    const result = await deleteHotComment(cid).then((data) => data);

    if (result.deletedCount > 0) {
      res = {
        code: "0000",
        msg: "删除成功",
        data: null,
      }
    } else {
      res = {
        code: "1003",
        msg: "删除失败",
        data: null,
      }
    }
  } else {
    const list = await getCommentsList(aid, cid, mark);
    if (list.length > 0) {
      const hostCommets = new HotCommentsModel();
      hostCommets.commentBody = list[0].commentBody;
      hostCommets.commentName = list[0].commentName;
      hostCommets.cid = list[0].cid;
      hostCommets.aid = aid;
      hostCommets.createTime = list[0].createTime;
      hostCommets.icon = list[0].icon;
      hostCommets.title = title;
      hostCommets.mark = mark;
      const result = await hostCommets.save().then((data) => data);

      if (result.aid === aid) {
        res = {
          code: "0000",
          msg: "评论成功",
          data: null,
        };
      } else {
        res = {
          code: "1003",
          msg: "评论失败",
          data: null,
        };
      }
    } else {
      res = {
        code: "1001",
        msg: "评论失败",
        data: null,
      };
    }
  }

  const isHot = hotResult !== null ? '0' : '1';
  if(res.code === "0000"){
    await CommentsModel.findOneAndUpdate(
      { aid: aid, "commentList.cid": cid },
      { $set: { "commentList.$.isHot": isHot} },
    ).then(data =>data);
  }
  ctx.body = res

})

//获取热门评论列表
router.get("/get-comments-hot", async (ctx, next) => {
  const result = await getHotComments().then((data) => data);
  ctx.body = {
    code: "0000",
    msg: "获取成功",
    data: result,
  };

})



module.exports = router;
