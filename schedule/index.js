// * * * * * *
// | | | | | |
// | | | | | day of week
// | | | | month
// | | | day of month
// | | hour
// | minute  30 5 * * *
// second ( optional )
//定时任务
const cron = require("node-cron");

const OverviewWork = require("./overviewWork")
const NotesCount = require("./notesCount")
const TravelCount = require("./travelCount")

const schedule = async()=>{
  //每分钟的定时任务
  cron.schedule(" 30 * * * *", () => {
    NotesCount()
    TravelCount()
    console.log('定时任务启动');
  });
  //每日的定时任务
  cron.schedule("30 5 * * *", () => {
    OverviewWork()
    console.log('每日定时任务');
  });
}

module.exports = schedule();
