const OverviewModel = require("../model/overviewModel");
const NotesModel = require("../model/notesModel");
const TravelListModel = require("../model/travelListModel");
const dayjs = require("dayjs");
const returnNowDate = () => {
  return dayjs(new Date()).format("YYYY/MM/DD");
};
const Work = async () => {
  const notes = await NotesModel.find({}).then((data) => data);
  const travel = await TravelListModel.find({}).then((data) => data);
  const OldOverview = await OverviewModel.find({
    date: dayjs(new Date().getTime() - 24 * 60 * 60 * 1000).format(
      "YYYY/MM/DD"
    ),
  }).then((data) => data);
  let sumNotes = 0;
  let sumTravel = 0;
  notes.map((item) => {
    if (isNaN(Number(item.watchNum))) {
      return;
    }
    sumNotes += Number(item.watchNum);
    return;
  });
  travel.map((item) => {
    if (isNaN(Number(item.watchNum))) {
      return;
    }
    sumTravel += Number(item.watchNum);
    return;
  });

  let overviewModel = new OverviewModel();
  overviewModel.notesWatch = sumNotes;
  overviewModel.travelWatch = sumTravel;
  if (OldOverview.length > 0) {
    overviewModel.dayOfNotesWatch =
    sumNotes - OldOverview[0].notesWatch;
    overviewModel.dayOfTravelWatch =
      sumTravel - OldOverview[0].travelWatch;
  } else {
    overviewModel.dayOfNotesWatch = sumNotes;
    overviewModel.dayOfTravelWatch = sumTravel;
  }
  overviewModel.date = returnNowDate();
  overviewModel.save().then((data) => {
    console.log("overview点击总数完成");
  });
};

module.exports = Work;
