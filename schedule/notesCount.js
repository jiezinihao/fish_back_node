const NotesNavsModel = require("../model/notesNavsModel");
const NotesModel = require("../model/notesModel");

const redisClient = require("../redis");
const NotesCount = async () => {
  //存储每个栏目下的文章数
  // const navList = await NotesNavsModel.find();
  const notesNav = await NotesNavsModel.find();
  const notes = await NotesModel.find();
  if (notesNav.length === 0) {
    return;
  }
  let notesMsg = [];
  notesNav.map((item, index) => {
    notesMsg.push({
      nav_id: item.nav_id,
      maxCount: 0,
    });
    for (let i = 0; i < notes.length; i++) {
      if (notes[i].nav_id === item.nav_id) {
        notesMsg[index].maxCount++;
      }
    }
    return;
  });

  redisClient.set("NotesCount", JSON.stringify(notesMsg));
};

module.exports = NotesCount;
