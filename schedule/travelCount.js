const TravelListModel = require("../model/travelListModel");

const redisClient = require("../redis");
const TravelCount = async () => {
  //存储每个栏目下的文章数
  const travel = await TravelListModel.find();

  
  redisClient.set("TravelCount",travel.length);
};

module.exports = TravelCount;
