const mongoose = require("mongoose")
const Schema = mongoose.Schema
const NotesNavs = {
    title:String,
    nav_id:String,
}
const NotesNavsModel = mongoose.model("notesnavs",new Schema(NotesNavs))

module.exports = NotesNavsModel
