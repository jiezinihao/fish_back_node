const mongoose = require("mongoose")
const Schema = mongoose.Schema
const Crons = {
    NotesCount:{
        nav_id:String,
        maxCount:String
    }
}
const CronsModel = mongoose.model("crons",new Schema(Crons))

module.exports = CronsModel