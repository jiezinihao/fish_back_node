const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const TravelImg = {
  uid: String,
  url: String,
  name: String,
};
const TravelLists = {
  title: String,
  body: String,
  travel_id: String,
  thumb: TravelImg,
  time: String,
  watchNum:String,
  proportion:Number,
  imgList: [TravelImg],
};
const TravelListsModel = mongoose.model("travelLists", new Schema(TravelLists));

module.exports = TravelListsModel;
