const mongoose = require("mongoose")
const Schema = mongoose.Schema
const Travel = {
    id:String,
    img:String,
    title:String,
    subtitle:String,
    time:Date,
    proportion:Number,
    coffee:Number
}
const TravelModel = mongoose.model("travel",new Schema(Travel))

module.exports = TravelModel