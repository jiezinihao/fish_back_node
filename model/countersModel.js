const mongoose = require("mongoose")
const Schema = mongoose.Schema
const counters = {
    id:String,
    sequence_value:Number,
}
const CountersModel = mongoose.model("counters",new Schema(counters))
module.exports = CountersModel
