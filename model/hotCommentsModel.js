const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const HotComments = {
    commentBody: String,
    commentName: String,
    cid: String,
    aid:String,
    createTime: String,
    icon: String,
    title:String,
    mark:String
};
const HotCommentsModel = mongoose.model("hotComments", new Schema(HotComments));

module.exports = HotCommentsModel;
