const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const CommentItem = {
  commentBody: String,
  commentName: String,
  cid: String,
  icon: String,
  createTime: String,
};
const CommentItemFrist = {
  commentBody: String,
  commentName: String,
  cid: String,
  createTime: String,
  icon: String,
  isHot:String, // 是否设置为精选评论
  commentBack: [CommentItem],
};

const Comment = {
  id: String,
  aid: String, //关联id
  mark: String,
  commentList: [CommentItemFrist],
};
const Comments = mongoose.model("comments", new Schema(Comment));

module.exports = Comments;
