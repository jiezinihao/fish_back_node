const mongoose = require("mongoose")
const Schema = mongoose.Schema
const Overview= {
    notesWatch:Number,
    travelWatch:Number,
    dayOfNotesWatch:Number,
    dayOfTravelWatch:Number,
    date:String,
    // notesAdd:String,
    // travelAdd:String,
}
const OverviewModel = mongoose.model("overviews",new Schema(Overview))

module.exports = OverviewModel
