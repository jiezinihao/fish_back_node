const mongoose = require("mongoose")
const Schema = mongoose.Schema
const Notes = {
    title:String,
    time:String,
    filePath:String,
    nav_id:String,
    article_id:String,
    watchNum:String,
    proportion:Number,
}
const NotesModel = mongoose.model("notes",new Schema(Notes))

module.exports = NotesModel