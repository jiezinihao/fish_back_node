var createError = require("http-errors");
var Koa = require("koa");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const router = require("./routes");
const static = require("koa-static");
const path = require("path");
const connect = require("./db/db");
const redisClient = require("./redis");
// const bodyParser = require("koa-bodyparser");
const cors = require("@koa/cors");
const { koaBody } = require('koa-body');
connect.then(() => {});
const app = new Koa();
require('./func/aliyun')
require('./schedule/index')


let dotenv =  require('dotenv');
dotenv.config('./env');
app.use(
  koaBody({
    multipart: true,
    formidable: {
      //上传文件存储目录
      uploadDir: path.join(__dirname, `/file/`),
      //允许保留后缀名
      keepExtensions: true,
      multipart: true,
    },
  })
);
// app.use(bodyParser());
app.use(cors()); //使用cors中间件
let opts = {
  maxage: 2592000000, //静态资源30天缓存 实际上 = 2592000秒
};
// app.use(static(path.join(__dirname, "public")));
app.use(static(path.join(__dirname, "file"),opts));



app.use(router.routes()).use(router.allowedMethods());
app.listen(3001);

module.exports = app;

// var app = new Koa();
// app.use(logger("dev"));

// app.use(cookieParser());
// app.use(bodyParser());
// app.use(static(path.join(__dirname, "public")));
// app.use(router.routes()).use(router.allowedMethods());

// app.use(function (req, res, next) {
//   next(createError(404));
// });

// // error handler
// app.use(function (err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get("env") === "development" ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render("error");
//   next()
// });
// app.listen(3000);
// module.exports = app;
// const app = new Koa();
// // app.use(logger("dev"));
// app.use(cookieParser());
// app.use(bodyParser());
// app.use(static(path.join(__dirname, "public")));
// app.use(router.routes()).use(router.allowedMethods());
// app.use(async ctx => {
//   ctx.body = 'Hello World';
// });
