const redis = require('redis') // 引入 redis

const redisClient = redis.createClient() // 创建客户端

// 监听错误信息
redisClient.on('err', err => {
  console.log('redis client error: ', err)
})
redisClient.connect(6379, "127.0.0.1").then(()=>{
    console.log('redis connect');
})
// 连接
module.exports = redisClient
