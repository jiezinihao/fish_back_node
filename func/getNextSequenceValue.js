const CountersModel = require("../model/countersModel");

const getNextSequenceValue = async (sequenceName) => {
  const result = await CountersModel.findOne({ id: sequenceName }).then(
    (data) => data
  );
  if (!result) {
    let countersModel = new CountersModel();
    countersModel.id = sequenceName;
    countersModel.sequence_value = 1;
    return await countersModel.save().then((data) => {
      return 1;
    });
  } else {
    const value = await CountersModel.findOneAndUpdate(
      { id: sequenceName },
      { $inc: { sequence_value: 1 } },
      { new: true }
    ).then((data) => data.sequence_value);
    return value
  }
};
module.exports = getNextSequenceValue;
