const svgCaptcha = require("svg-captcha"); // 引入模块
const captcha =  function(){
    let s = svgCaptcha.create({
        size: 4, //验证码长度
        noise: 1, //干扰线条数目
        width: 140, //宽度
        height: 38, //高度
        inverse: false, // 翻转颜色
        fontSize: 65, // 字体大小
        color: true, // 验证码字符颜色（需设置背景色）
        background: "#ccc", // 背景
      });
      return  s
}
module.exports = captcha
// 生成验证码的接口
// exports.svgCode = (req, res) => {
//   const captcha =
//   return Captcha
//   code = captcha.text; // 存储验证码数值（这里使用变量来存储生成的验证码，其实可以使用session来进行存储）
//   res.type('svg'); // 响应的类型
//   res.send(captcha.data);
// };
